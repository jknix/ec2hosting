	#!/bin/bash
	#install R
	# Add something here to update the ect/apt/sources.list to include 
	# deb https://cloud.r-project.org//bin/linux/ubuntu precise/
	# So that we get a reasonably up to date R distro
	sudo apt install r-base-core

	#install RStudio-Server 1.1.383
	wget https://download2.rstudio.org/rstudio-server-rhel-1.1.383-x86_64.rpm
	sudo apt-get install r-base
	sudo apt-get install psmisc
	sudo apt-get install openssl openssl-dev
	sudo apt-get install libcurl4-openssl-dev
	sudo apt-get install libxml2
	
	sudo apt-get install gdebi-core
	wget https://download2.rstudio.org/rstudio-server-1.1.383-amd64.deb
	sudo gdebi rstudio-server-1.1.383-amd64.deb
	
	# Get required R packages for shiny server
	sudo su - \
	-c "R -e \"install.packages('shiny', repos='https://cran.rstudio.com/')\""
	sudo su - \
	-c "R -e \"install.packages('rmarkdown', repos='https://cran.rstudio.com/')\""
	sudo su - \
	-c "R -e \"install.packages('RCurl', repos='https://cran.rstudio.com/')\""
	sudo su - \
	-c "R -e \"install.packages('aws.s3', repos='https://cran.rstudio.com/')\""
	wget https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.5.872-amd64.deb
	sudo gdebi shiny-server-1.5.5.872-amd64.deb

	